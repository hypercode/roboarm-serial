#include "Roka.h"
#include "Arduino.h"

Roka::Roka(int pin_servo_sklep[JOINTS]) {
  for (int i = 0; i < JOINTS; i++) {
    this->Sklep[i].attach(pin_servo_sklep[i]);
  }
  this->write(DEAFAULT_SKLEP_0, DEAFAULT_SKLEP_1, DEAFAULT_SKLEP_2, DEAFAULT_SKLEP_3, DEAFAULT_SKLEP_4, DEAFAULT_SKLEP_5);
}




void Roka::write(float a, float b, float c, float d, float e, float f) {
  float angles[JOINTS] = {a, b, c, d, e, f};
  this->write(angles);
}




/* Main servo write function! */
void Roka::write(float angles[JOINTS]) {
  for (int i = 0; i < JOINTS; i++) {
    if (kot[i] != angles[i]) {
      this->Sklep[i].write(angles[i]);
    }
    this->kot[i] = angles[i];
#ifdef VERBOSE_DEBUG
    Serial.print("Writing ");
    Serial.print(angles[i]);
    Serial.print("° to servo: ");
    Serial.println(i);
#endif
  }
}





void Roka::write(int sklep, float angle) {
  float newAngles[JOINTS];
  for (int i = 0; i < JOINTS; i++) {
    if (i != sklep) {
      newAngles[i] = this->kot[i];
    }else{
      newAngles[i] = angle;
    }
  }
  this->write(newAngles);
}






