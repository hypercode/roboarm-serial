#include <Servo.h>
#include "Arduino.h"

#define DEAFAULT_SKLEP_0 90
#define DEAFAULT_SKLEP_1 90
#define DEAFAULT_SKLEP_2 90
#define DEAFAULT_SKLEP_3 90
#define DEAFAULT_SKLEP_4 90
#define DEAFAULT_SKLEP_5 90

#define JOINTS 6
#define VERBOSE_DEBUG

class Roka {
  public:
    Roka(int pin_servo_sklep[JOINTS]);

    void write(float a, float b, float c, float d, float e, float f);

    void write(float angles[JOINTS]);

    void write(int sklep, float angle);

  private:
    Servo Sklep[JOINTS];
    float kot[JOINTS];
};







